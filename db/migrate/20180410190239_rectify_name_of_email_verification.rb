class RectifyNameOfEmailVerification < ActiveRecord::Migration[5.1]
  def change
  	rename_column :emails, :validation_token, :verification_token
  	rename_column :emails, :validated, :verified
  end
end
