class CreateVerificationSendLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :verification_send_logs do |t|
    	t.integer :email_id
    	t.timestamps
    end
  end
end
