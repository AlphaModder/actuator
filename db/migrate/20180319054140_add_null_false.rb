class AddNullFalse < ActiveRecord::Migration[5.1]
  def change
  	change_column_null :items, :title, false
  	change_column_null :items, :threshold, false
  	change_column_null :items, :success_msg, false 

  	change_column_null :emails, :email, false 
  	change_column_null :emails, :validated, false 
  	change_column_null :emails, :validation_token, false 
  	change_column_null :emails, :item_id, false
  end
end
