class RenameToPasswordDigest < ActiveRecord::Migration[5.1]
  def change
    rename_column :items, :password_hash, :password_digest
  end
end
