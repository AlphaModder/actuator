class AddToItems < ActiveRecord::Migration[5.1]
  def change
  	add_column :items, :threshold, :integer
  	add_column :items, :password_hash, :string
  	add_column :items, :success_msg, :text
  end
end
