require 'rails_helper'

RSpec.feature 'End date' do
  it 'disables the signup form after it passes' do
    item = FactoryBot.build(:item, end_date: Date.today)
    item.save(validate: false)
    visit item_path(item)
    expect(page).to_not have_content('Add your email')
  end
end
