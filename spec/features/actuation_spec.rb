require 'rails_helper'
include ActiveJob::TestHelper
require 'support/helpers/features/actuation_helper'

RSpec.feature 'Actuation', type: :mailer do
  let (:item) { FactoryBot.create(:item, success_msg: 'Success!') }

  before :each do
    ActionMailer::Base.deliveries = []
  end

  it 'sends a success message when an item reaches its threshold' do
    Helpers::Actuation.create_verified_emails(item, 1)
    expect(ActionMailer::Base.deliveries.first).to have_content('Success!')
  end

  it 'ignores unverified signups' do
    Helpers::Actuation.create_emails(item, 5)
    expect(ActionMailer::Base.deliveries.count).to eq 0
  end

  it 'sends success messages to all verified signups' do
    Helpers::Actuation.create_verified_emails(item, 5)
    expect(ActionMailer::Base.deliveries.count).to eq 5
  end

  it 'sends success messages to only verified signups' do
    Helpers::Actuation.create_verified_emails(item, 5)
    Helpers::Actuation.create_emails(item, 5)
    expect(ActionMailer::Base.deliveries.count).to eq 5
  end

  it 'doesn\'t resend success emails' do
    Helpers::Actuation.create_verified_emails(item, 1) # in two separate calls so it checks succeeded? twice
    Helpers::Actuation.create_verified_emails(item, 1)
    expect(ActionMailer::Base.deliveries.count).to eq 2
  end
end
