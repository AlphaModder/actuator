module Helpers
  module Removal
    extend Capybara::DSL
    class << self
      delegate :url_helpers, to: 'Rails.application.routes'
      def verify(email)
        email.verified = true
        email.save!
      end

      def request_removal(email, item)
        visit url_helpers.item_path(item)
        fill_in 'email_remove', with: email
        perform_enqueued_jobs { click_on 'Send removal request' }
      end
    end
  end
end
