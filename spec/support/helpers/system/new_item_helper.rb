module Helpers
  module NewItem
    extend Capybara::DSL
    class << self
      delegate :url_helpers, to: 'Rails.application.routes'
      def build_new_item(params = {}, &block)
        visit url_helpers.new_item_path
        fill_in 'Title',       with: params.key?(:title)       ? params[:title]       : 'Test title'
        fill_in 'Description', with: params.key?(:description) ? params[:description] : 'Test description'
        fill_in 'Threshold',   with: params.key?(:threshold)   ? params[:threshold]   : 1
        fill_in 'Password',    with: params.key?(:password)    ? params[:password]    : 'foo'
        fill_in 'Success msg', with: params.key?(:success_msg) ? params[:success_msg] : 'Test msg'
        yield if block
        click_button 'Create Item'
      end
    end
  end
end
