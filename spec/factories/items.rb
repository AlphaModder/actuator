FactoryBot.define do
  factory :item do
    title { Faker::Company.bs }
    description { Faker::Lorem.paragraph }
    threshold { 1 }
    password_digest { BCrypt::Password.create('foo') }
    success_msg { 'Success!' }
  end
end
