require 'rails_helper'

RSpec.describe Item do
  it 'has a valid factory' do
    expect(FactoryBot.create(:item)).to be_valid
  end

  it 'has many emails' do
    item = FactoryBot.create(:item)
    FactoryBot.create(:email, item: item)
    FactoryBot.create(:email, item: item)
    expect(item.emails.count).to be 2
  end

  it 'distinguishes between verified and unverified emails' do
    item = FactoryBot.create(:item)
    email = FactoryBot.create(:email, item: item)
    email.verified = true
    email.save!
    FactoryBot.create(:email, item: item)
    expect(item.verified_signups.count).to be 1
  end

  it 'deletes its dependents when it is deleted' do
    # shouldn't need deletion functionality at all once it's live
    item = FactoryBot.create(:item)
    5.times { FactoryBot.create(:email, item: item) }
    item_two = FactoryBot.create(:item)
    4.times { FactoryBot.create(:email, item: item_two) }
    item.destroy
    expect(Email.count).to be 4
  end
end
