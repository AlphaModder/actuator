require 'rails_helper'

RSpec.describe Email do
  it 'has a valid factory' do
    expect(FactoryBot.create(:email)).to be_valid
  end

  it 'belongs to an item' do
    item = FactoryBot.create(:item)
    email = FactoryBot.create(:email, item: item)
    expect(email.item).to eq(item)
  end

  it 'requires an item' do
    expect(FactoryBot.build(:email, item: nil)).to_not be_valid
  end

  it 'is unique per item' do
    item = FactoryBot.create(:item)
    FactoryBot.create(:email, item: item, email: 'foo@bar.baz')
    expect { FactoryBot.create(:email, item: item, email: 'foo@bar.baz') }.to raise_error(ActiveRecord::RecordNotUnique)
  end

  it 'is unverified by default' do
    expect(FactoryBot.create('email').verified).to be false
  end
end
