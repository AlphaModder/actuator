class UserMailer < ApplicationMailer
  after_action { SendLog.create(email: @email) }

  default from: 'coordinationtoolthrowaway@gmail.com'

  def verification_email(email)
    @email = email
    @url = verify_url(@email.id, @email.verification_token)
    @item = email.item
    mail(to: @email.email, subject: 'Verification')
  end

  def removal_email(email)
    @email = email
    @url = remove_url(@email.id, @email.verification_token)
    @item = email.item
    mail(to: @email.email, subject: "Signup removal request from #{@item.title}")
  end

  def success_email(email)
    item = email.item
    @success_msg = item.success_msg
    mail(to: email.email, subject: "Threshold met: #{item.title}")
  end
end
