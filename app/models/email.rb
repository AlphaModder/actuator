class Email < ApplicationRecord
  belongs_to :item
  has_many :send_logs
  before_create :set_defaults
  validates :item, presence: true
  validate :item_not_ended

  def verify
    self.verified = true
    save
  end

  def send_verification
    # we don't want adversaries to be able to determine whether an email is signed up for something
    # but we also don't want to enable any send-pizza attacks
    # so if you've tried to sign up three times already in the last 24 hours it'll fail silently
    # this could maybe be done better but it'll do for now #agile
    return unless can_send?
    UserMailer.verification_email(self).deliver_later
  end

  def send_removal_request
    return unless verified && can_send?
    UserMailer.removal_email(self).deliver_later
  end

  def send_success_msg
    UserMailer.success_email(self).deliver_later unless sent_success_msg
    update(sent_success_msg: true)
  end

  private

    def can_send?
      SendLog.where(email: self, created_at: (24.hours.ago..Time.now)).count < 3
    end

    def set_defaults
      self.verification_token = SecureRandom.hex
      self.verified = false
    end

    def item_not_ended
      if item && item.end_date && item.end_date == Date.today
        errors.add(:item_end_date, "can't be today")
      elsif item && item.end_date && item.end_date < Date.today
        errors.add(:item_end_date, "can't be in the past")
      end
    end
end
