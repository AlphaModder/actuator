class EmailsController < ApplicationController
  before_action :set_email

  def verify
    if @email && @email.verify
      item = @email.item
      item.succeeded?
      redirect_to item_url(item), notice: 'Verified'
    else
      redirect_to items_url, notice: 'Error - can\'t verify'
    end
  end

  def remove
    if @email
      item = @email.item
      @email.destroy
      redirect_to item_url(item), notice: 'You have been removed from this item'
    else
      redirect_to items_url, notice: 'Error - can\'t remove this email'
    end
  end

  private

    def set_email
      @email = Email.find_by(id: params[:id], verification_token: params[:verification_token])
    end
end
