Rails.application.routes.draw do
  resources :items, only: [:index, :create, :new, :edit, :show, :update]
  post '/items/:id/sign_up', to: 'items#sign_up', as: 'sign_up'
  post '/items/:id/password', to: 'items#input_password', as: 'item_input_password'
  post '/items/:id/remove', to: 'items#remove', as: 'item_remove'
  get '/emails/:id/:verification_token', to: 'emails#verify', as: 'verify'
  get '/emails/:id/:verification_token/remove', to: 'emails#remove', as: 'remove'
  root to: 'items#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
